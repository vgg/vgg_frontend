#!/usr/bin/env python

import multiprocessing
import os
import threading
import time

import imsearchtools.postproc_modules.module_finder
from imsearchtools.engines.bing_api_v5 import BingAPISearchV5
from imsearchtools.engines.flickr_api import FlickrAPISearch
from imsearchtools.engines.google_api import GoogleAPISearch
from imsearchtools.engines.google_old_api import GoogleOldAPISearch
from imsearchtools.engines.google_web import GoogleWebSearch
from imsearchtools.process.image_getter import ImageGetter
from imsearchtools.process.image_processor import ImageProcessorSettings

from visorgen.controllers.retengine import models
from visorgen.controllers.retengine.managers import (
    compdata_cache as compdata_cache_module,
)
from visorgen.controllers.retengine.utils import timing


POSTRAINIMGS_DIRNAME = 'postrainimgs'


class ImsearchPipeline(multiprocessing.Process):
    def __init__(
            self,
            engine_name,
            query,
            query_params,
            getter_params,
            postproc_module_name,
            postproc_args,
            custom_local_path,
    ):
        super().__init__(daemon=False)
        self._engine_name = engine_name
        self._query = query
        self._query_params = query_params
        self._getter_params = getter_params
        self._postproc_module_name = postproc_module_name
        self._postproc_args = postproc_args
        self._custom_local_path = custom_local_path

    def run(self):
        _engine_name_to_cls = {
            "bing_api": imsearchtools.engines.BingAPISearchV5,
            "google_old_api": imsearchtools.engines.GoogleOldAPISearch,
            "google_api": imsearchtools.engines.GoogleAPISearch,
            "google_web": imsearchtools.engines.GoogleWebSearch,
            "flickr_api": imsearchtools.engines.FlickrAPISearch,
        }
        searcher = _engine_name_to_cls[self._engine_name]()
        search_results = searcher.query(self._query, **self._query_params)
        print(
            "Query for '%s' completed: %d results retrieved"
            % (self._query, len(search_results))
        )

        getter = ImageGetter(**self._getter_params)
        os.makedirs(self._custom_local_path, exist_ok=True)

        callback_func = imsearchtools.postproc_modules.module_finder.get_module_callback(
            self._postproc_module_name
        )
        get_results = getter.process_urls(
            search_results,
            self._custom_local_path,
            callback_func,
            completion_extra_prms=self._postproc_args
        )
        print(
            "Downloading for %s completed: %d images retrieved"
            % (self._query, len(get_results))
        )


class ImsearchResultsMonitor(threading.Thread):
    """Monitor images downloaded by the image search provider.

    While imsearchtools downloads and processes images, we want to
    show them to the user so they see progress.  imsearchtools
    downloads and processes images in a separate process.  We could
    set up a queue for the image paths.  However, since we specify the
    directory path, we can just monitor that directory for new files.
    """

    def __init__(self, imsearch_finished: threading.Event, custom_local_path, shared_vars):
        super().__init__()
        self._imsearch_finished = imsearch_finished
        self._custom_local_path = custom_local_path
        self._shared_vars = shared_vars

        diridx = self._custom_local_path.find(POSTRAINIMGS_DIRNAME)
        self._rel_outdir = self._custom_local_path[diridx-1:]

        self._current_files = set()


    def run(self):
        """Stores the paths to the downloaded training images in the
            holder of global shared variables.
        """
        while not self._imsearch_finished.is_set():
            time.sleep(1)
            previous_files = self._current_files
            self._current_files = set(os.listdir(self._custom_local_path))

            new_files = self._current_files.difference(previous_files)
            for fname in new_files:
                if not fname.endswith("-clean.jpg"):
                    continue

                new_img_relpath = os.path.join(self._rel_outdir, fname)
                # XXX: for some reason, if we use `append` instead of
                # `+=` the list remains empty (maybe it's not a list?)
                self._shared_vars.postrainimg_paths += [new_img_relpath]


class TextQuery(object):
    """
        Class for performing text queries.

        This query invokes the imsearchtool to download training images
        from the web, by using the query string as input to an image
        search engine.

        If a particular text engine does not support images as input,
        the downloading step can be skipped by configuring the engine
        with its 'imgtools_postproc_module' set to 'None'.
    """

    def __init__(self, query_id, query, backend_port, compdata_cache, opts):
        """
            Initializes the class.
            Arguments:
                query_id: id of the query being executed.
                query: query in dictionary form
                backend_port: Communication port with the backend
                compdata_cache: Computational data cache manager.
                opts: current configuration of options for the visor engine
            Returns:
                It raises ValueError in case of incorrect options or
                computational cache
        """
        if not isinstance(opts, models.param_sets.VisorEngineProcessOpts):
            raise ValueError('opts must be of type models.param_sets.VisorEngineProcessOpts')
        if not isinstance(compdata_cache, compdata_cache_module.CompDataCache):
            raise ValueError('compdata_cache must be of type managers.CompDataCache')

        self.query_id = query_id
        self.query = query
        self.backend_port = backend_port
        self.compdata_cache = compdata_cache
        self.opts = opts


    def compute_feats(self, shared_vars):
        """
            Performs the computation of features.
            Arguments:
                shared_vars: holder of global shared variables
            Returns:
                The time it took to download the trainig images and process them.
                If the 'imgtools_postproc_module' of the search engine is set to
                'None', it will return zero.
        """

        # Check that a imgtools_postproc_module has actually been configured ...
        if self.compdata_cache.engines_dict[self.query['engine']]['imgtools_postproc_module'] != None:

            # get output image directory from compdata_cache
            imagedir = self.compdata_cache.get_image_dir(self.query)
            featdir = self.compdata_cache.get_feature_dir(self.query)

            # Clear out some previous images
            self.compdata_cache.cleanup_unused_query_postrainimgs_cache(self.query)

            # call imsearchtool service
            return self._call_imsearchtool(imagedir, featdir, shared_vars)

        else:

            # ... and do not download images otherwise
            return 0


    def _call_imsearchtool(self, imagedir, featdir, shared_vars):
        """
            Calls the imsearchtool service to download positive training images and send them
            to the backend for processing.
            Arguments:
                imagedir: path to the folder where the downloaded images will be stored
                featdir: path to the folder where the computed features will be stored
                shared_vars: holder of global shared variables
            Returns:
                The time it took to download the training images and process them.
        """
        engine_cfg = self.compdata_cache.engines_dict[self.query["engine"]]

        engine_name = self.opts.imsearchtools_opts['engine']
        query = self.query['qdef']
        query_params = {
            "style": engine_cfg["imgtools_style"],
            "num_results": self.opts.imsearchtools_opts['num_pos_train'],
        }

        improc_settings = ImageProcessorSettings()
        improc_settings.conversion["max_width"] = self.opts.resize_width
        improc_settings.conversion["max_height"] = self.opts.resize_height

        improc_timeout = self.opts.imsearchtools_opts['improc_timeout']
        # if the engine settings specify a timeout, use that one instead
        if 'improc_timeout' in engine_cfg:
            improc_timeout = engine_cfg['improc_timeout']
        # when using the 'accurate' detector, double the image processing timeout
        if self.opts.feat_detector_type == models.opts.FeatDetectorType.accurate:
            improc_timeout = 2* improc_timeout

        getter_params = {
            "timeout": improc_timeout,
            "image_timeout": self.opts.imsearchtools_opts["per_image_timeout"],
            "opts" : improc_settings,
        }

        postproc_module_name = engine_cfg['imgtools_postproc_module']
        postproc_args = {
            'func': 'addPosTrs',
            'backend_host': 'localhost',
            'backend_port': self.backend_port,
            'query_id': self.query_id,
            'featdir': featdir,
            'detector' : self.opts.feat_detector_type
        }
        custom_local_path = imagedir

        imsearch_finished_event = threading.Event()
        query_monitor = ImsearchResultsMonitor(
            imsearch_finished_event, custom_local_path, shared_vars
        )
        query_monitor.start()

        print('Query ID for current query: %d' % self.query_id)
        with timing.TimerBlock() as timer:
            try:
                query_process = ImsearchPipeline(
                    engine_name,
                    query,
                    query_params,
                    getter_params,
                    postproc_module_name,
                    postproc_args,
                    custom_local_path,
                )
                query_process.start()
                query_process.join(timeout=10*60)  # times out at 10m
            except multiprocessing.TimeoutError:
                print ('Image download and feature computation timed out for query:', self.query)
            finally:
                if query_process.exitcode is None:
                    print(
                        "ImsearchPipeline for query '%s' still running.  Will kill process."
                        % self.query
                    )
                    query_process.kill()

        imsearch_finished_event.set()
        query_monitor.join()

        comp_time = timer.interval
        print ('Done with call to imsearchtool for Query ID:', self.query_id)

        return comp_time
