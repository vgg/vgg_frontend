$(function() {

    $('#selection_save_via_csv').bind('click', function() {
        var list = '';
        var qsid = '';
        var page = '';
        $('.roi_box').each(function(i, obj) {
            if ($(this).attr('anno') == 1 ) {
                list = list + $(this).attr('id') + ';';
                qsid = $(this).attr('qsid');
                page = $(this).attr('page');
            }
        });
        if (list=='') {
            alert('No image has been selected for saving');
        }
        else {
            savePageAsViaCSV(list, qsid, page);
        }
    });

    $('#set_all_images_yellow').bind('click', function() {
        $('.roi_box').each(function(i, obj) {
            div_ele = $(this);
            div_ele.attr('anno', '0');
            div_ele.removeClass('roi_box_positive');
            div_ele.addClass('roi_box_skip');
        });
    });

    $('#set_all_images_green').bind('click', function() {
        $('.roi_box').each(function(i, obj) {
            div_ele = $(this);
            div_ele.attr('anno', '1');
            div_ele.removeClass('roi_box_skip');
            div_ele.addClass('roi_box_positive');
        });
    });

});

function savePageAsViaCSV(list, qsid, page) {
    fullHomeLocation = location.protocol + '//' + window.location.hostname;
    if (location.port.length>0) {
        fullHomeLocation = fullHomeLocation + ':' + location.port + '/';
    }
    else {
        fullHomeLocation = fullHomeLocation + '/';
    }
    // Check if there is any site prefix to add to the request.
    const last_slash = location.pathname.lastIndexOf('/');
    if (last_slash >= 1)
        fullHomeLocation += location.pathname.substring(1, last_slash +1);

    execstr = fullHomeLocation + 'save_results_as_via_csv?qsid=' + qsid +
        '&list=' + encodeURIComponent(list) +
        '&page=' + page;
    window.open(execstr, '_blank');
}
